//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "NewDigi.h"
#include "ProjectNewDigi.h"
#include "NewDigiProjects.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
#pragma resource ("*.LgXhdpiPh.fmx", _PLAT_ANDROID)

TForm1Dashboard *Form1Dashboard;
//---------------------------------------------------------------------------
__fastcall TForm1Dashboard::TForm1Dashboard(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------


void __fastcall TForm1Dashboard::SpeedButton1Click(TObject *Sender)
{
	Single targetX;

	targetX = ClientWidth - 60;

	if(targetX == Panel1->Position->X)
	{
		 AnimationDrawer->StartValue = targetX;
		 AnimationDrawer->StopValue = 0;
	}
	else
	{
		AnimationDrawer->StartValue = 0;
		 AnimationDrawer->StopValue = targetX;
	}

	AnimationDrawer->Start();

}
//---------------------------------------------------------------------------

void __fastcall TForm1Dashboard::Menu1ProjectsClick(TObject *Sender)
{

	Form2Projects->Show();

	Single targetX;

	targetX = ClientWidth - 60;


	 AnimationDrawer->StartValue = targetX;
	 AnimationDrawer->StopValue = 0;



}
//---------------------------------------------------------------------------


void __fastcall TForm1Dashboard::FormCreate(TObject *Sender)
{
	RESTRequest1->Execute();
}
//---------------------------------------------------------------------------

