//---------------------------------------------------------------------------

#ifndef NewDigiH
#define NewDigiH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Ani.hpp>
#include <FMX.Effects.hpp>
#include <FMX.Objects.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.ObjectScope.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <IPPeerClient.hpp>
#include <REST.Client.hpp>
#include <REST.Response.Adapter.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
//---------------------------------------------------------------------------
class TForm1Dashboard : public TForm
{
__published:	// IDE-managed Components
	TListBox *ListBox1;
	TListBoxGroupHeader *HeaderMenu1;
	TListBoxItem *Menu1Projects;
	TListBoxItem *Menu1Contacts;
	TListBoxGroupHeader *HeaderMenu2;
	TListBoxItem *Menu2Events;
	TListBoxItem *Menu2Dashboard;
	TListBoxItem *Menu2News;
	TPanel *Panel1;
	TToolBar *ToolBar1;
	TSpeedButton *SpeedButton1;
	TFloatAnimation *AnimationDrawer;
	TShadowEffect *ShadowEffect1;
	TText *Text1;
	TRESTClient *RESTClient1;
	TRESTRequest *RESTRequest1;
	TRESTResponse *RESTResponse1;
	TListView *ListView1;
	TRESTResponseDataSetAdapter *RESTResponseDataSetAdapter1;
	TFDMemTable *FDMemTable1;
	TWideStringField *FDMemTable1ws_success;
	TWideStringField *FDMemTable1ws_error;
	TWideStringField *FDMemTable1ws_error_code;
	TWideStringField *FDMemTable1ws_error_message;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkListControlToField *LinkListControlToField1;
	void __fastcall SpeedButton1Click(TObject *Sender);
	void __fastcall Menu1ProjectsClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1Dashboard(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1Dashboard *Form1Dashboard;
//---------------------------------------------------------------------------
#endif
