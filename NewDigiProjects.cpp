//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "NewDigiProjects.h"
#include "NewDigi.h"


#include <System.JSON.hpp>

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
#pragma resource ("*.NmXhdpiPh.fmx", _PLAT_ANDROID)
#pragma resource ("*.LgXhdpiPh.fmx", _PLAT_ANDROID)

TForm2Projects *Form2Projects;
//---------------------------------------------------------------------------
__fastcall TForm2Projects::TForm2Projects(TComponent* Owner)
	: TForm(Owner)
{

}
//---------------------------------------------------------------------------
void __fastcall TForm2Projects::SpeedButton1Click(TObject *Sender)
{
	Single targetX;

	targetX = ClientWidth - 60;

	if(targetX == Panel1->Position->X)
	{
		 AnimationDrawer->StartValue = targetX;
		 AnimationDrawer->StopValue = 0;
	}
	else
	{
		AnimationDrawer->StartValue = 0;
		 AnimationDrawer->StopValue = targetX;
	}

	AnimationDrawer->Start();
}
//---------------------------------------------------------------------------

void __fastcall TForm2Projects::Menu2DashboardClick(TObject *Sender)
{
	Form1Dashboard->Show();

	Single targetX;

	targetX = ClientWidth - 60;


		 AnimationDrawer->StartValue = targetX;
		 AnimationDrawer->StopValue = 0;


//         AnimationDrawer->StartValue = 0;
//		 AnimationDrawer->StopValue = targetX;

	//AnimationDrawer->Start();
}
//---------------------------------------------------------------------------

void __fastcall TForm2Projects::SpeedButton2Click(TObject *Sender)
{
	// Create the outer JSON object which parents the others.
  TJSONObject *o = new TJSONObject();
  __try {
		// Create the books object, which contains the array of books...
		TJSONArray *a = new TJSONArray();

		// add the array to the object.
		o->AddPair("books",a);

		// Create the first book
		TJSONObject *book = new TJSONObject();
		book->AddPair( new TJSONPair("title","Zen") );
		book->AddPair( new TJSONPair("subtitle","and The art of motorcycle maintenance.") );
		book->AddPair( new TJSONPair("author","Robert M Pirsig") );
		book->AddPair( new TJSONPair("isbn","9780061908019") );
		// Add the book to the array
		a->AddElement(book);

		// Create the second book
		book = new TJSONObject();
		book->AddPair( new TJSONPair("title","Coding in Delphi") );
		book->AddPair( new TJSONPair("subtitle","") );
		book->AddPair( new TJSONPair("author","Nick Hodges") );
		book->AddPair( new TJSONPair("isbn","978-1941266038") );
		// Add the book to the array
		a->AddElement(book);
	}
  __finally
	{
	  Memo1->Lines->Text = o->ToString();
	  o->Free();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm2Projects::SpeedButton3Click(TObject *Sender)
{
	 TJSONObject *o = (TJSONObject*) TJSONObject::ParseJSONValue(TEncoding::ASCII->GetBytes(Memo1->Lines->Text),0);
  __try {
	TJSONArray *a = (TJSONArray*) o->Get("books")->JsonValue;
	for (int idx = 0; idx < a->Size(); idx++) {
	  TJSONObject *book = (TJSONObject*) a->Get(idx);
	  for (int idy = 0; idy < book->Count; idy++) {
		ShowMessage( book->Pairs[idy]->JsonString->ToString() + ':' +
					 book->Pairs[idy]->JsonValue->ToString() );
	  }
	}
  }
  __finally {
	o->Free();
  }
}
//---------------------------------------------------------------------------

