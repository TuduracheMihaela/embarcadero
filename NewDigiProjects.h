//---------------------------------------------------------------------------

#ifndef NewDigiProjectsH
#define NewDigiProjectsH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Ani.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class TForm2Projects : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TText *Text1;
	TSpeedButton *SpeedButton1;
	TPanel *Panel1;
	TFloatAnimation *AnimationDrawer;
	TListBox *ListBox1;
	TListBoxGroupHeader *HeaderMenu1;
	TListBoxItem *Menu1Projects;
	TListBoxItem *Menu1Contacts;
	TListBoxGroupHeader *HeaderMenu2;
	TListBoxItem *Menu2Events;
	TListBoxItem *Menu2Dashboard;
	TListBoxItem *Menu2News;
	TSpeedButton *SpeedButton2;
	TMemo *Memo1;
	TSpeedButton *SpeedButton3;
	void __fastcall SpeedButton1Click(TObject *Sender);
	void __fastcall Menu2DashboardClick(TObject *Sender);
	void __fastcall SpeedButton2Click(TObject *Sender);
	void __fastcall SpeedButton3Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm2Projects(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm2Projects *Form2Projects;
//---------------------------------------------------------------------------
#endif
